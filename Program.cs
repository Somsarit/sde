﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using SpectroDataExport.Helpers;

namespace SpectroDataExport
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string sourceFolder = ConfigurationManager.AppSettings["SourceFolder"];
        private static readonly string backupFolder = ConfigurationManager.AppSettings["BackupFolder"];
        private static readonly string outputFolder = ConfigurationManager.AppSettings["OutputFolder"];

        static void Main(string[] args)
        {
            FileWatcher(sourceFolder, OnSourceFileCreated);
            FileWatcher(outputFolder, OnOutputFileCreated);

            Console.WriteLine("Enter any key to exit.");
            Console.ReadLine();
        }

        static public void FileWatcher(string path, FileSystemEventHandler handler)
        {
            // Create a new FileSystemWatcher and set its properties.
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;
            /* Watch for changes in LastAccess and LastWrite times, and 
               the renaming of files or directories. */
            watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            // Only watch text files.
            watcher.Filter = "*.txt";

            // Add event handlers.
            watcher.Created += new FileSystemEventHandler(handler);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }

        // Define the event handlers.
        private static void OnSourceFileCreated(object source, FileSystemEventArgs e)
        {
            if (new Helpers.FileManager(log).ReadReportFile(e.FullPath))
            {
                string backupFile = String.Format("{0}{1}", backupFolder, "backup_" + e.Name);
                File.Move(e.FullPath, backupFile);
                log.Info(String.Format("Move {0} to backup folder.", e.Name));
            }
        }

        // Define the event handlers.
        private static void OnOutputFileCreated(object source, FileSystemEventArgs e)
        {
            using (SerialPortConnector serialPort = new SerialPortConnector())
            {
                new Helpers.FileManager(log).ProcessOutputFile(e.FullPath, serialPort);
            }

        }
    }
}
