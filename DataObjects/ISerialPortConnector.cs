﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpectroDataExport.DataObjects
{
    public interface ISerialPortConnector
    {
        bool Write(string line);
    }
}
