﻿using SpectroDataExport.DataObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpectroDataExport.Helpers
{
    public class SerialPortConnector : ISerialPortConnector, IDisposable
    {
        StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
        
        SerialPort _serialPort;

        public SerialPortConnector()
        {
            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            _serialPort.PortName = SetPortName(_serialPort.PortName);
            _serialPort.BaudRate = SetPortBaudRate(_serialPort.BaudRate);
            _serialPort.Parity = SetPortParity(_serialPort.Parity);
            _serialPort.DataBits = SetPortDataBits(_serialPort.DataBits);
            _serialPort.StopBits = SetPortStopBits(_serialPort.StopBits);
            _serialPort.Handshake = SetPortHandshake(_serialPort.Handshake);

            _serialPort.Open();
        }

        public bool Write(string s)
        {
            var result = false;
            try { 
                _serialPort.WriteLine(s);
                Console.WriteLine("Send data success");
                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("Send data failed: {0}", ex.Message));
            }
            return result;
        }

        // Display Port values and prompt user to enter a port.
        private string SetPortName(string defaultPortName)
        {
            string portName = String.Empty;

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Com:PortName"]))
                portName = ConfigurationManager.AppSettings["Com:PortName"];

            if (portName == "" || !(portName.ToLower()).StartsWith("com"))
            {
                portName = defaultPortName;
            }
            return portName;
        }

        // Display BaudRate values and prompt user to enter a value.
        private int SetPortBaudRate(int defaultPortBaudRate)
        {
            string baudRate = String.Empty;

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Com:BaudRate"]))
                baudRate = ConfigurationManager.AppSettings["Com:BaudRate"];

            if (baudRate == "")
            {
                baudRate = defaultPortBaudRate.ToString();
            }

            return int.Parse(baudRate);
        }

        // Display PortParity values and prompt user to enter a value.
        private Parity SetPortParity(Parity defaultPortParity)
        {
            string parity = String.Empty;

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Com:Parity"]))
                parity = ConfigurationManager.AppSettings["Com:Parity"];

            if (parity == "")
            {
                parity = defaultPortParity.ToString();
            }

            return (Parity)Enum.Parse(typeof(Parity), parity, true);
        }
        // Display DataBits values and prompt user to enter a value.
        private int SetPortDataBits(int defaultPortDataBits)
        {
            string dataBits = String.Empty;

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Com:DataBits"]))
                dataBits = ConfigurationManager.AppSettings["Com:DataBits"];

            if (dataBits == "")
            {
                dataBits = defaultPortDataBits.ToString();
            }

            return int.Parse(dataBits.ToUpperInvariant());
        }

        // Display StopBits values and prompt user to enter a value.
        private StopBits SetPortStopBits(StopBits defaultPortStopBits)
        {
            string stopBits = String.Empty;

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Com:StopBits"]))
                stopBits = ConfigurationManager.AppSettings["Com:StopBits"];

            if (stopBits == "")
            {
                stopBits = defaultPortStopBits.ToString();
            }

            return (StopBits)Enum.Parse(typeof(StopBits), stopBits, true);
        }
        private Handshake SetPortHandshake(Handshake defaultPortHandshake)
        {
            string handshake = String.Empty;

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Com:HandShake"]))
                handshake = ConfigurationManager.AppSettings["Com:HandShake"];

            if (handshake == "")
            {
                handshake = defaultPortHandshake.ToString();
            }

            return (Handshake)Enum.Parse(typeof(Handshake), handshake, true);
        }

        public void Dispose()
        {
            if (_serialPort != null)
                _serialPort.Close();
        }
    }

    
}
