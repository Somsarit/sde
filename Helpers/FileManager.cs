﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Configuration;
using SpectroDataExport.DataObjects;
using log4net;

namespace SpectroDataExport.Helpers
{
    class FileManager
    {
        ILog _log;

        public FileManager(ILog log)
        {
            _log = log;
        }

        public bool ReadReportFile(string filePath)
        {
            int row = 0;
            StringBuilder sb = new StringBuilder();
            string outputFile = String.Format("{0}{1}"
                                              , ConfigurationManager.AppSettings["OutputFolder"]
                                              , ConfigurationManager.AppSettings["OutputFileFormat"]);

            outputFile = outputFile.Replace("{timestamp}", DateTime.Now.ToString("yyyyMMddHHmmss"));

            using (StreamReader sr = File.OpenText(filePath))
            {
                Console.WriteLine(String.Format("Read file: {0}", filePath));

                string s = String.Empty;

                //Read first line
                s = sr.ReadLine();
                string[] headers = ParseHeader(s);

                //Read the rest of the content
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.StartsWith("Average"))
                    {
                        string line = ParseLine(s, headers);

                        if (!String.IsNullOrEmpty(line))
                            sb.AppendLine(line);
                    }
                    else
                    {
                        continue;
                    }

                    row++;
                }
                _log.Info("Read file success.");
            }

            using (StreamWriter sw = new StreamWriter(outputFile, true, Encoding.UTF8))
            {
                _log.Info("Write output file: " + outputFile);
                sw.WriteLine(sb);
            }

            return true;
        }

        public bool ProcessOutputFile(string filePath, ISerialPortConnector serialPort)
        { 

            using (StreamReader sr = File.OpenText(filePath))
            {
                int row = 0;
                Console.WriteLine(String.Format("Read output file: {0}", filePath));
                _log.Info(String.Format("Read output file: {0}", filePath));

                string line = String.Empty;
                

                //Read the rest of the content
                while ((line = sr.ReadLine()) != null)
                {
                    row++;

                    if (String.IsNullOrEmpty(line))
                        continue;

                    _log.Info(String.Format("Send line {0}: {1}", row, line));

                    serialPort.Write(line);
                }
                Console.WriteLine("Send file send file success");
                _log.Info(String.Format("Send file send file success, total: {0} lines", row));
            }
            return true;
        }

        private string[] ParseHeader(string headerText)
        {
            string[] result = headerText.Split('\t');//.Select(h => h.Trim()).Where(s => s != string.Empty).ToArray();


            return result;
        }

        private string ParseLine(string lineText, string [] headers)
        {
            string[] result = lineText.Split('\t');//.Select(h => h.Trim()).Where(s => s != string.Empty).ToArray();
            string lineTemplate = ConfigurationManager.AppSettings["DataTemplate"];
            string elementTemplate = ConfigurationManager.AppSettings["ElementTemplate"];
            int prefixLen = Convert.ToInt32(ConfigurationManager.AppSettings["PrefixLen"]);
            int yearLen = Convert.ToInt32(ConfigurationManager.AppSettings["YearLen"]);
            int methodLen = Convert.ToInt32(ConfigurationManager.AppSettings["MethodLen"]);
            int sampleNoLen = Convert.ToInt32(ConfigurationManager.AppSettings["SampleNoLen"]);
            int sampleSourceLen = Convert.ToInt32(ConfigurationManager.AppSettings["SampleSourceLen"]);
            int heatNoLen = Convert.ToInt32(ConfigurationManager.AppSettings["HeatNoLen"]);
            int elementLen = Convert.ToInt32(ConfigurationManager.AppSettings["ElementLen"]);
            StringBuilder sb = new StringBuilder();

            string line = lineTemplate.Replace("{prefix}", "06341007150714141NJL".PadRight(prefixLen, ' '))
                                      .Replace("{year}", result[1].Trim().Substring(6, 4).PadRight(yearLen, ' '))
                                      .Replace("{method}", result[2].Trim().PadRight(methodLen, ' '))
                                      .Replace("{heat no}", result[4].Trim().PadRight(heatNoLen, ' '))
                                      .Replace("{sample no}", result[5].Trim().PadRight(sampleNoLen, ' '))
                                      .Replace("{sample source}", result[6].Trim().PadRight(sampleSourceLen, ' '));

            StringBuilder element = new StringBuilder();
            for (int c = 12; c < result.Count(); c++)
            {
                if (result[c].Trim().Equals(String.Empty))
                    continue;

                sb.Append(elementTemplate.Replace("{value}", result[c].Trim())
                                         .Replace("{element}", headers[c].Trim())
                                         .PadRight(elementLen, ' '));
            }

            line = line.Replace("{elements}", sb.ToString());

            return line;
        }
    }
}
